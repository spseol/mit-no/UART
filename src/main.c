#include "main.h"
#include "milis.h"
#include <stdbool.h>
#include <stdio.h>
#include <stm8s.h>

void init(void) {
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // taktovani MCU na 16MHz

    GPIO_Init(LED_PORT, LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(BTN_PORT, BTN_PIN, GPIO_MODE_IN_FL_NO_IT);

    init_milis();

    // inut UART1
    UART1_DeInit();
    UART1_Init(9600,                         // modulační rychlost
               UART1_WORDLENGTH_8D,          // délka slova je 8b
               UART1_STOPBITS_1,             // jeden stopbit
               UART1_PARITY_NO,              // žádný praritní bit
               UART1_SYNCMODE_CLOCK_DISABLE, // jedeme v asynchronní módu
               UART1_MODE_TXRX_ENABLE        // budeme vysílat i přijímat
    );

    UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
    enableInterrupts();
}

unsigned char motd[80] = "Stale jsem zde...";
unsigned char string[80];

void char_process(char c)
{
    static uint8_t i = 0;

    if (c == '\n') {
        printf("\n Bylo zadáno toto: >%s<\n\n", string);

        // proměnnou string nakopíruju do proměnné motd
        uint8_t k = 0;
        while (k < 80) {
            motd[k] = string[k];
            if (string[k++] == '\0')
                break;
        }
        unsigned char *u = string, *v = motd;
        string[79] = '\0';
        while ((  *v++ = *u++));

        i = 0;
        string[i] = '\0';
    } else if (c == '\r') {
        return;
    } else {
        string[i++] = c;
        string[i] = '\0';
    }
}

#if defined(_SDCC_)
int putchar(int c) // hlavička pro kompilátor SDCC
#elif defined(_COSMIC_)
char putchar(char c) // hlavička pro kompilátor COSMIC
#endif
{
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET)
        ;
    UART1_SendData8(c);
    return c;
}

int main(void) {

    uint32_t time = 0;
    char c;

    init();
    printf("Start:\n\n");

    while (1) {
        /*
         * tohle je v poznámce protože používám přerušení
         * 
        if (UART1_GetFlagStatus(UART1_FLAG_RXNE)) {
            c = UART1_ReceiveData8();
            char_process(c);
            REVERSE(LED);
        } */

        if (milis() - time > 2222) {
            time = milis();

            { // pošlu jeden znak
                while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET)
                    ;
                UART1_SendData8('\n');
            }
            putchar(':');  // pošlu jeden znak
            printf(" >%s<\n", motd); // ... aby fungoval printf musí fungovat putchar
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
